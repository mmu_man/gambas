��    #      4  /   L           	                    8     ?     G     N     c     z     �  (   �     �     �     �     �  	   �     �     �  &   �     &     /     2     >     K     T  	   Z     d     m     s  6   y     �     �  ,   �     �     �               $     C     J  	   L     V     s     �     �  5   �  
   �     �     
               %     +     2     A     O     Q     ^     m     z     �  
   �     �  
   �  =   �     �       %                         #                                 !          "                                                             
      	                         &Close &Reset &Undo '&1' toolbar configuration Action Button1 Cancel Configure &1 toolbar Configure main toolbar Configure shortcuts Configure... Do you really want to reset the toolbar? Expander Find shortcut Go back Help Icon size Large Medium Multiple document interface management Next tab OK Orientation Previous tab Reparent Reset Separator Shortcut Small Space This shortcut is already used by the following action: Toolbar configuration Window You are going back to the default shortcuts. Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 &Schließen Zu&rücksetzen &Rückgängig Werkzeugleiste '&1' einrichten Aktion - Abbrechen Werkzeugleiste &1 einrichten Hauptwerkzeugleiste einrichten Kurzbefehle einrichten Einrichten... Wollen Sie die Werkzeugleiste wirklich zurücksetzen? Erweiterer Kurzbefehl suchen Zurück Hilfe Icon-Größe Groß Mittel MDI-Management Nächster Tab - Orientierung Vorheriger Tab Neu zuordnen Zurücksetzen Trenner Kurzbefehl Klein Leerstelle Dieser Kurzbefehl ist bereits mit der folgenen Aktion belegt: Werkzeugleiste einrichten Fenster Zurück zu den Standard-Kurzbefehlen. 
��    r      �  �   <      �	     �	     �	     �	     �	     �	     �	     �	     
     
     "
     (
     4
     =
     E
     V
  (   X
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
               7  	   =     G     L     ]     i     q  	        �  !   �  ,   �  '   �  +        A     F     U     \     l     ~     �     �     �     �     �  #   �     �     �     �     �     �     �  &   �     !     0     =  
   B     M  
   R     ]     `  	   y     �     �     �     �     �     �     �     �     �     �     �     �     �     
          %     '     5     H  
   \     g     l     x  	   �     �  >   �  &   �     �     �     
          +  	   B     L     ]     `     b     h     t     z     �     �     �  �   �     �     �     �     �     �     �     �  
   �     �               %  	   1     ;     U  ;   W     �     �  	   �     �     �     �     �     �  $   �  ,   
  &   7  "   ^  '   �     �     �     �     �     �     �     �     �     
     #  1   ?  0   q  /   �     �     �     �     �     
     '  	   )     3     :     <     D  -   V     �     �     �     �     �     �  &   �  !   �  "        A     E     Y     a     n  +   q     �     �     �     �     �     �     �     �                  	        %     A     T     j     }          �     �     �     �     �     �     
       L     +   g     �     �     �  '   �  #   �  	   �                         &     3     <     N     \  
   k     $      n      :   [   l   6       W           G      "      P                 q   9   !       '       8                 O               m   F       )   3   i   ]             r   _           >   `   T   =       o       2   #   R       5                     b      ,   1   4   X   7   	   +   e   L   N             E       B   k   a   U                 /       g   C       Q   *             Z   ^   V   ?       D   h      I   <   J   \                  S   ;   @   0   K   .   d   Y       p              f   A           j   &         -         H      (       
           %   c      M                &1 B &1 GiB &1 KiB &1 MiB &1 properties &Bookmark current directory &Create directory &Delete &Edit bookmarks... &Next &Properties &Refresh &Rename &Uncompress file A A file or directory name cannot be void. Add item All files (*) Apply Audio B Bold Bookmarks Cancel Cannot create directory. Cannot list archive contents Cannot rename directory. Cannot rename file. Cannot uncompress file. Close ComboBox1 Copy Delete directory Delete file Desktop Detailed view Directory Directory not found. Do not display this message again Do you really want to delete that directory? Do you really want to delete that file? Do you really want to remove this bookmark? Down Edit bookmarks Errors File properties Follow color grid G General Group H Hidden Home How quickly daft jumping zebras vex Image Image preview Italic Last colors Last modified Link More controls for graphical components Move item down Move item up Name New folder Next Next month OK Open in &file manager... Overwrite Overwrite all Owner Parent directory Path Permissions Preview Previous Previous month R Relative Remove Remove all colors Remove color Remove item Root directory S Show &details Show &hidden files Show &image preview Show files Size Sort colors Step #&1 Strikeout System The '/' character is forbidden inside file or directory names. This file or directory already exists. Today Transparent Type Unable to delete directory. Unable to delete file. Underline Unknown archive. Up V Video directories files no directory no file one directory one file Project-Id-Version: gb.form 3.11.90
PO-Revision-Date: 2018-07-17 21:10 UTC
Last-Translator: benoit <benoit@benoit-kubuntu>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 &1 o &1 Gio &1 Kio &1 Mio Propriétés de &1 &Marquer le répertoire courant &Créer un répertoire &Supprimer &Editer les signets... Suivant &Propriétés &Actualiser &Renommer &Décompresser le fichier - Le nom d'un fichier ou d'un répertoire ne peut être vide. Ajouter un élément Tous les fichiers (*) Appliquer Audio - Gras Signets Annuler Impossible de créer le répertoire. Impossible de lister le contenu de l'archive Impossible de renommer le répertoire. Impossible de renommer le fichier. Impossible de décompresser le fichier. Fermer - Copier Supprimer le répertoire Supprimer le fichier Bureau Vue détaillée Répertoire Répertoire introuvable. Ne plus afficher ce message Désirez-vous vraiment supprimer ce répertoire ? Désirez-vous réellement supprimer ce fichier ? Désirez-vous réellement supprimer ce signet ? Bas Edition des signets Erreurs Propriétés du fichier Suivre la grille de couleurs - Général Groupe - Masqué Dossier personnel Portez ce vieux whisky au juge blond qui fume Image Prévisualisation des images Italique Dernières couleurs Modifié le Lien Contrôles graphiques supplémentaires Déplacer l'élément vers le bas Déplacer l'élément vers le haut Nom Nouveau répertoire Suivant Mois suivant OK Ouvrir dans le gestionnaire de &fichiers... Ecraser Tout écraser Propriétaire Répertoire parent Emplacement Permissions Aperçu Précédent Mois précédent - Relatif Supprimer Retirer toutes les couleurs Retirer la couleur Supprimer l'élément Répertoire racine - Vue &détaillée Afficher les fichiers &cachés Prévisualisation des &images Afficher les fichiers Taille Trier les couleurs Étape n°&1 Barré Système Le caractère '/' est interdit dans les noms de fichiers ou de répertoires. Ce fichier ou ce répertoire existe déjà. Aujourd'hui Transparent Type Impossible de supprimer le répertoire. Impossible de supprimer le fichier. Souligné Archive inconnue. Haut - Vidéo répertoires fichiers aucun répertoire aucun fichier un répertoire un fichier 
��            )   �      �     �     �     �     �  !   �     �     �     �     �     �                    $     -     5     :     L     T     Z     _     t     �     �     �     �     �  #   �                    '  	   /  (   9     b     f     k     ~  !   �  
   �     �     �     �  
   �     �  #   �          $     +     0  )   M      w  #   �  &   �  #   �       /                                                            	                               
                                                  Blob contents Cancel Clear Delete Do you really want to clear blob? End False Incorrect value. Invalid value. Load blob from file Load... New Next Previous Refresh Save Save blob to file Save... Start True Unable to clear blob Unable to delete record. Unable to load file Unable to save file Unable to save record. Unable to save value. Unknown You must fill all mandatory fields. Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Contenu du blob Annuler Effacer Supprimer Désirez-vous vraiment effacer le blob ? Fin Faux Valeur incorrecte. Valeur incorrecte. Charger le blob depuis un fichier Charger... Nouveau Suivant Précédent Actualiser Enregistrer Enregistrer le blob dans un fichier Enregistrer... Début Vrai Impossible d'effacer le blob Impossible de supprimer l'enregistrement. Impossible de charger le fichier Impossible d'enregistrer le fichier Impossible de sauver l'enregistrement. Impossible d'enregistrer la valeur. Inconnu Vous devez saisir tous les champs obligatoires. 
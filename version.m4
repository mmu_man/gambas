## Package version and e-mail for bugs are defined here

m4_define([GB_VERSION], [3.11.90])
m4_define([GB_MAIL], [gambas@users.sourceforge.net])
m4_define([GB_URL], [http://gambas.sourceforge.net])

m4_define([GB_VERSION_MAJOR], [3])
m4_define([GB_VERSION_MINOR], [11])
m4_define([GB_VERSION_RELEASE], [90])

m4_define([GB_VERSION_FULL], [0x03110090])
m4_define([GB_PCODE_VERSION], [0x03080000])
m4_define([GB_PCODE_VERSION_MIN],[0x03000000])


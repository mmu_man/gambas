��    M      �  g   �      �     �     �     �     �     �     �     �  	   �     �     �     �     �     �     �     �                         2     N     h     q  
   �     �     �     �     �  
   �     �     �     �     �  
   �     �     �     �  "        %     +     :      L  =   m  %   �  ,   �      �  '   	     G	  N   g	     �	     �	  %   �	     
     
     :
     P
     g
     
     �
  !   �
  *   �
     	     #     ?  V   D     �  /   �  	   �  
   �     �     �     �  	                  3  �   9     1     3     5     ;     C     L     S  
   f     q     w          �     �  "   �     �     �     �     �     �     �  %     
   3  #   >  
   b     m     t     y     �     �     �     �     �     �     �     �     �     �  !   �          
       %   6  A   \  +   �  5   �  &      0   '  #   X  h   |     �     �  )        :  "   M     p     �     �     �     �  '   �  &        D     ^     y  [   �     �  7   �     +     4     @     I     O     S     b     u     �         ;              %             ,   @      $       +       >             A   H       F                    7                     9   !       <   B   3   C      E              8   #      0             )   '   :   6   	               J   4       "   L       D      ?                  .   I           &       *   =      1   /   (          -   K             5         
   G   M   2    < > Author Authors Cancel Class Complex numbers Constants Create Delete Description Edit Errors Event loop management Events Examples Excludes Exit Gambas Documentation Gambas Wiki Web Application Graphical form management Historic Image loading and saving Implements Login Logout Message Methods No change. OK OpenGL display Password Preview Properties Requires Save See also Select the image file to upload... Since Static methods Static properties The english page is more recent. There are &1 classes and &2 symbols in all Gambas components. This class acts like a &1 / &2 array. This class acts like a &1 / &2 static array. This class acts like a &1 array. This class acts like a &1 static array. This class can be used as a &1. This class can be used like an object by creating a hidden instance on demand. This class does not exist. This class inherits This class is &1 with the &2 keyword. This class is &1. This class is not creatable. This class is static. This class is virtual. This class reimplements This component does not exist. This component is deprecated. This component is not stable yet. This page does not exist in that language. This page does not exist. This symbol does not exist. Undo Warning! This is a preview. Click on the <b>&1</b> button to go back to the edit page. XML management You must be logged in to view the last changes. creatable enumerable function in &1 read read-only static function statically enumerable write Project-Id-Version: gambas-wiki 3.8.4
PO-Revision-Date: 2016-01-05 12:05 UTC
Last-Translator: Edison Henrique Andreassy <ehasis@hotmail.com>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 - - Autor Autores Cancelar Classe Números complexos Constantes Criar Excluir Descrição Editar Erros Gerenciaomento do laço de eventos Eventos Exemplos Exclui Sair Documentação do Gambas Aplicação Web Wiki Gambas Gerenciamento de formulário gráfico Histórico Carregamento e salvamento de imagem Implementa Entrar Sair Mensagem Métodos Sem mudança. - Exibição OpenGL Senha Prever Propriedades Requer Salvar Veja também Selecione a imagem para upload... Desde Métodos estáticos Propriedades estáticas A página em inglês é mais recente. Existem &1 classes e &2 símbolos em todos os componentes Gambas. Esta classe funciona como um array &1 / &2. Esta classe funciona como um array estático &1 / &2. Esta classe funciona como um array &1. Esta classe funciona como um array estático &1. Esta classe pode ser usada como &1. Esta classe pode ser usada como um objeto através da criação de uma instância escondida sob demanda. Esta classe não existe. Esta classe herda Esta classe é &1 com &2 palavras chaves. Esta classe é &1. Esta classe não é instanciável. Esta classe é estática. Esta classe é virtual. Esta classe reimplementa Este componente não existe. Este componente é obsoleto. Este componente ainda não é estável. Esta página não existe neste idioma. Esta página não existe. Este símbolo não existe. Desfazer Aviso! Isto é uma previsão. Clique no botão <b>&1</b> para voltar a página de edição. Gerenciamento de XML Você deve estar logado para ver as últimas mudanças. criável enumerável função em &1 ler apenas leitura função estática enumerável estaticamente escrever 
��    N      �  k   �      �     �     �     �     �     �     �     �  	   �     �     �     �     �          	          &     /     8     =     R     n     �     �  
   �     �     �     �     �  
   �     �     �     �     �  
                        "  "   +     N     T     c      u  =   �  %   �  ,   �      '	  '   H	     p	  N   �	     �	     �	  %   
     4
     F
     c
     y
     �
     �
     �
  !   �
  *        2     L     h  V   m     �  /   �  	     
             !     '  	   ,     6     F     \  �   b     N     P     R     X     `     i     o  
   �     �     �     �     �     �     �     �     �     �     �     �          '  
   G     R  
   m     x     �     �     �     �     �     �     �     �     �  	   �     �     �       )        ;     A     U  '   l  @   �  +   �  5     &   7  /   ^  (   �  R   �     
        )   3     ]     o     �     �     �     �     �  #     '   3     [     s     �  p   �       4        H     U     W     `     f     n     |     �  	   �                      
   >   (          "       3   #   !              H      *   0      $      K          ,          B   :               9                 C   N       J          4   )       1       E                 -   I      6      F   =   '             @   	                 %       ?   G      <                 L       +      2   A   ;           8   5   /   M         &       .       7   D       < > Author Authors Cancel Class Complex numbers Constants Create Delete Description Edit Errors Event loop management Events Examples Excludes Exit Gambas Documentation Gambas Wiki Web Application Graphical form management Historic Image loading and saving Implements Login Logout Message Methods No change. OK OpenGL display Password Preview Properties Register Requires Save See also Select the image file to upload... Since Static methods Static properties The english page is more recent. There are &1 classes and &2 symbols in all Gambas components. This class acts like a &1 / &2 array. This class acts like a &1 / &2 static array. This class acts like a &1 array. This class acts like a &1 static array. This class can be used as a &1. This class can be used like an object by creating a hidden instance on demand. This class does not exist. This class inherits This class is &1 with the &2 keyword. This class is &1. This class is not creatable. This class is static. This class is virtual. This class reimplements This component does not exist. This component is deprecated. This component is not stable yet. This page does not exist in that language. This page does not exist. This symbol does not exist. Undo Warning! This is a preview. Click on the <b>&1</b> button to go back to the edit page. XML management You must be logged in to view the last changes. creatable enumerable function in &1 read read-only static function statically enumerable write Project-Id-Version: gambas-wiki 3.8.90
PO-Revision-Date: 2015-09-20 17:36 UTC
Last-Translator: Jesus <ea7dfh@users.sourceforge.net>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 - - Autor Autores Cancelar Clase Números complejos Constantes Crear Borrar Descripción Editar Errores Gestión del lazo de eventos Eventos Ejemplos Excluye Salir Documentación de Gambas Aplicación Web Gambas Wiki Gestión gráfica de formulario Histórico Carga y guardado de imagen Implementa Iniciar sesión Salir Mensaje Métodos Sin cambios. - Visualización OpenGL Contraseña Vista previa Propiedades Registrar Requiere Guardar Ver también Seleccionar fichero de imagen a cargar... Desde Métodos estáticos Propiedades estáticas La página en inglés es más reciente. Hay &1 clases y &2 símbolos en todos los componentes de Gambas. Esta clase actúa como un array de &1 / &2. Esta clase actúa como un array estático de &1 / &2. Esta clase actúa como un array de &1. Esta clase actúa com un array estático de &1. Esta clase se puede usar como un/una &1. Esta clase se puede usar como un objeto creando una instancia oculta bajo demanda. Esta clase no existe. Esta clase hereda  Esta clase es &1 con la palabra clave &2. Esta clase es &1. Esta clase no es instanciable. Esta clase es estática. Esta clase es virtual. Esta clase reimplementa  Este componente no existe. Este componente es obsoleto. Este componente aún no es estable. Esta página no existe en ese lenguaje. Esta página no existe. Este símbolo no existe. Deshacer ¡Atención! Esto es una vista previa. Haz clic en el botón <b>&1</b> para retroceder a la página de edición. Gestión XML Debes iniciar sesión para ver los últimos cambios. instanciable - función en &1 lectura sólo lectura función estática enumerable estáticamente escritura 
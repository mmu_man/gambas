# Catalan translation of HTTPPost
# Copyright (C) 2000-2010 Benoît Minisini.
# This file is distributed under the same license as the HTTPPost package.
# Jordi Sayol <g.sayol@yahoo.es>, 2007-2010.
#
#
msgid ""
msgstr ""
"Project-Id-Version: HTTPPost\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: 2010-12-17 01:10+0100\n"
"Last-Translator: Jordi Sayol <g.sayol@yahoo.es>\n"
"Language-Team: \n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Language: Catalan\n"

#: F.form:114
msgid "127.0.0.1:3128"
msgstr "-"

#: F.form:63
msgid "5"
msgstr "-"

#: F.class:72
msgid "Connecting..."
msgstr "S'està connectant..."

#: F.class:7
msgid "Error "
msgstr "-"

#: F.form:73
msgid "Finally press the Post button >>>"
msgstr "Finalment premeu el botó Envia >>>"

#: .project:1
msgid "HTTP client POST example"
msgstr "Exemple POST de client HTTP"

#: F.form:48
msgid "HTTP Data"
msgstr "Dades HTTP"

#: F.form:42
msgid "HTTP Headers"
msgstr "Capçaleres HTTP"

#: F.form:23
msgid "Internet Calculator"
msgstr "Calculadora d'Internet"

#: F.form:28
msgid "Post"
msgstr "Envia"

#: F.form:108
msgid "Use Proxy"
msgstr "Usa servidor intermediari"

#: F.form:81
msgid "Using the power of internet to have a minimal calculator machine!"
msgstr "Usant el poder d'Internet per tenir una mínima màquina de calcular!"

#: F.class:22
msgid "Waiting for reply..."
msgstr "S'està esperant resposta..."

#: F.form:58
msgid "Write Here Another Number :"
msgstr "Escriviu aquí un altre nombre:"

#: F.form:53
msgid "Write Here a Number :"
msgstr "Escriviu aquí un nombre:"


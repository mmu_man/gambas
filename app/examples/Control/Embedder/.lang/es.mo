��          |      �             !     *  P   1  �   �          0     <  6   E  ,   |     �     �     �  	   �  	   �  ^   �  �   <  (   �       
   -  7   8  4   p     �     �               
             	                               &Discard &Embed Click on the <b>Discard</b> button to free the application window from its jail. Click on the <b>Embed</b> button to search the window whose title 
is specified in the left field, and to embed it in the 
blue rectangle below. Desktop application embedder Embed error Embedder Enter there the title of the window you want to embed. Several windows found. I take the first one! Window not found! Window title Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 &Retornar &Empotrar Haga click en el botón <b>Retornar</b> para liberar la ventana de la aplicación de su jaula. Haga click en el botón <b>Empotrar</b> para buscar la ventana cuyo título 
ha especificado en el campo de la izquierda 
con el fin de empotrarla en el rectángulo azul de abajo.  Empotrador de aplicaciones de escritorio Error de empotrado Empotrador Introduzca el título de la ventana que desea empotrar. Multiples ventanas encontradas. ¡Usando la primera! ¡Ventana no encontrada! Título de la ventana 
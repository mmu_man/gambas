��          �      �           	  ^   #     �     �     �  
   �     �     �     �  	   �  
   �     �  	   �     �               -     ;  	   Q     [     g  	   w     �     �  e   �       
   !  	   ,     6     G     Y  
   ^     i     k     w  
   ~     �     �     �     �     �       
              2              	                                        
                                                  A simple TextEdit example A simple TextEdit example by
Fabien Bodard (gambix@users.sourceforge.net)
and Benoît Minisini About this example... Align Center Align Justify Align Left Align Right Bold Close ComboBox1 HtmlSource Italic Read-only Set background color Set font color Set font face Set font size Show html source code StrikeOut Text Editor TextEditExample Underline Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Ein einfaches TextEdit-Beispiel Ein einfaches TextEdit-Beispiel von
Fabien Bodard (gambix@users.sourceforge.net)
und Benoît Minisini Über dieses Beispiel... Zentrieren Blocksatz Links ausrichten Rechts ausrichten Fett Schließen - HTML-Quelle Kursiv Nur lesbar Hintergrundfarbe auswählen Schriftfarbe auswählen Schriftart auswählen Schriftgröße auswählen HTML-Quellcode anzeigen Durchgestrichen Texteditor TextEdit-Beispiel Unterstrichen 
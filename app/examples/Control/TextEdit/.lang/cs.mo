��          �      �           	  ^   #     �     �     �  
   �     �     �     �  	   �  
   �     �  	   �     �               -     ;  	   Q     [     g  	   w     �  (   �  k   �          -     @     R     b     s     {     �  	   �     �     �     �     �     �     �          #     2     ?     \              	                                        
                                                  A simple TextEdit example A simple TextEdit example by
Fabien Bodard (gambix@users.sourceforge.net)
and Benoît Minisini About this example... Align Center Align Justify Align Left Align Right Bold Close ComboBox1 HtmlSource Italic Read-only Set background color Set font color Set font face Set font size Show html source code StrikeOut Text Editor TextEditExample Underline Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Jednoduchý příklad textového editoru Jednoduchý příklad textového editoru od
Fabien Bodard (gambix@users.sourceforge.net)
a Benoît Minisini O tomto příkladu... Zarovnat na střed Zaorvnat do bloku Zarovnat doleva Zarovnar doprava Tučné Zavřít - Html kód Kurzíva Pouze pro čtení Nastavit barvu pozadí Nastavit barvu textu Nastavit název písma Nastavit velikost textu Zobrazit zdrojový html kód Přeškrknuté Editor textu Příklad textového editoru Podtržené 
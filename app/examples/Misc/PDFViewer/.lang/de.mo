��          �      <      �  e   �  ;        S  	   \     f  
   v  	   �     �     �     �     �  	   �     �     �     �     �     �     �  d   �  G   _  	   �     �     �     �     �     �     �                    "     2  '   9     a     n           
                                                                	                          <h2>Simple PDF document viewer</h2><p>Gambas example by Daniel Campos Fernández  and Bernd Brinkmann A simple PDF Viewer, used as example of gb.pdf capabilities About... Find text Go to this page Next Found Next page OK Open file... Page Pdf documents PdfViewer Previous page Rotate Simple PDF document viewer Zoom in Zoom out Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 <h2>Einfacher PDF-Betrachter</h2><p>Gambas-Beispiel von Daniel Campos Fernández und Bernd Brinkmann Ein einfacher PDF-Betrachter, Beispiel für die Fähigkeiten von gb.pdf &Über... Text finden Gehe zu Seite Nächste Fundstelle Nächste Seite - Datei öffnen... Seite pdf-Dateien PDF-Betrachter Vorherige Seite Drehen Einfacher Betrachter für PDF-Dokumente Vergrößern Verkleinern 
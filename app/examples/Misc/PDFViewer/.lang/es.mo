��          �      �       H  ;   I  	   �     �  	   �     �     �  	   �     �     �     �  I   �     2     :  �   C  D   9     ~     �     �     �     �  	   �     �     �     �  S   �     M  	   V                              	         
                          A simple PDF Viewer, used as example of gb.pdf capabilities Find text From Next page OK Open file... PdfViewer Previous page Rotate Simple PDF document viewer Simple PDF document viewer.<p>Gambas example by Daniel Campos Fernández  Zoom in Zoom out Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Un simple Visor PDF, usado como ejemplo de las capacidades de gb.pdf Buscar texto de Página siguiente Aceptar Abrir archivo... Visor Pdf Página anterior Rotar Visor simple de documentos PDF Visor simple de documentos PDF.<p>Ejemplo para Gambas por Daniel Campos Fernández. Aumentar Disminuir 